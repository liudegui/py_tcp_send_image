import socket
import struct, base64
import sys
import time

def pack():
    id = 1234
    content = "Here are some brief details. To begin with, there is no doubt that you should be familiar with the professional skills as soon as possible. What is more, in order to make a deep impression on your manager, I do recommend that you arrive at your office in advance every day. Finally, it is also important to maintain a good relationship with your colleagues."
    content = content.encode(encoding="utf-8")
    content_size = len(content)
    with open('beng.jpg','rb') as f:
        img = base64.b64encode(f.read())
    img_size = len(img)
    ss = struct.pack('>IIII%ds%ds%ds' % (content_size, img_size, img_size), id, content_size, img_size, img_size, content, img, img)
    return ss

if __name__ == '__main__':
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(('192.21.1.43', 8701))
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 10*1024*1024)
    count =0
    ss = pack()
    while True:
        sock.send(ss)
        count += 1
        print('count: ', count)
        #time.sleep(0.02)
        
    